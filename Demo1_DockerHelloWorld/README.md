# docker installation testen
docker run hello-world

# container von fertigem image starten
# name für container
# portmapping [hostport:containerport/protokoll]
# image
docker run --name nginx -p 80:80/tcp nginx
http://localhost/

# Ctrl-C

# auf laufenden container verbinden
docker exec -it nginx bash

# container auflisten
docker ps

# container stoppen
docker stop nginx

# container löschen
docker rm nginx
