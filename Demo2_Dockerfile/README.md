# cd currentdirectory

# image aus Dockerfile builden
docker build -t nginxcustom .

# lokale images anzeigen
docker images

# neues image ausführen
docker run --name nginxcustom -p 80:80 nginxcustom

# Ctrl-C

# container stoppen und entfernen
docker stop nginxcustom
docker rm nginxcustom

# image mit bind-mount starten (read-write demonstration) [hostVerzeichnis:containerVerzeichnis]
# ${PWD} = current working directory - relative Pfade funktionieren hier nicht
docker run --name nginxcustom -p 80:80 -v ${PWD}/HTML:/usr/share/nginx/html nginxcustom

# HTML Datei im Host-Verzeichnis ändern --> änderungen direkt im nginx ersichtlich

# ins image connecten
docker exec -it nginxcustom bash

# unter /usr/share/nginx/html mit
# touch test123.txt
# eine Datei anlegen --> im Host-Verzeichniss sofort ersichtlich

# container stoppen und entfernen
docker stop nginxcustom
docker rm nginxcustom