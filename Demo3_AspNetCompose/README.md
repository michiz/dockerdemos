# cd currentdirectory!

# hier benutzen wir einen öffentlichen container um ein Demo .ASP-NET Core 2.1 MVC projekt runterzuladen
<!-- docker run -v ${PWD}/src:/app --workdir /app mcr.microsoft.com/dotnet/sdk:5.0 dotnet new mvc --auth Individual -->

# Dockerfile zeigen

# entrypoint.sh 

# appsettings.json:
"DefaultConnection": "Server=db;Database=master;User=sa;Password=myPassw0rd!;trustServerCertificate=true;"

# Startup.cs:
UseSqlServer
//app.UseHttpsRedirection();

# app.csproj, add:
<PackageReference Include="Microsoft.EntityFrameworkCore.SqlServer" Version="5.0.0-rc.2.20475.6" />

# images builden  --> Dauert 100s wegen timout dotnet restore
# dazwischen Dockerfile zeigen, entrypoint etc.
docker-compose build

# komposition starten
docker-compose up

# user einfügen under http://localhost:80

# exec into db container:
docker exec -it demo3_aspnetcompose_db_1 bash

# start sqlcmd:
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P myPassw0rd!

# show user table:
SELECT * FROM master.dbo.AspNetUsers
GO