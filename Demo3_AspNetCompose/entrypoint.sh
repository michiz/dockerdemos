#!/bin/bash

export PATH="$PATH:$HOME/.dotnet/tools/"

sleep 15

dotnet ef migrations add InitialCreate
dotnet ef database update --verbose

set -e
run_cmd="dotnet run --urls http://*:80"

exec $run_cmd